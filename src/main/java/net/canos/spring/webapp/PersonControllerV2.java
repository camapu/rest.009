package net.canos.spring.webapp;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController(value="PersonControllerV2")
@RequestMapping("/person")
public class PersonControllerV2 {
	Logger log = Logger.getLogger(getClass());

	@Autowired
	private PersonService personService;

		
	/*HATEOAS Example */
	@RequestMapping(value="/{personId}",method=RequestMethod.GET,headers= {"X-API-Version=3"})
	public ResponseEntity<?> getV4(@PathVariable("personId") Integer personId) {
		log.info("GET v2");
		PersonDTO person = personService.findById(personId);
		PersonDTOv2 personV2 = new PersonDTOv2(person);
		
		personV2.add(linkTo(methodOn(PersonControllerV2.class).getV4(personId)).withSelfRel());
		personV2.add(linkTo(methodOn(PersonControllerV2.class).create(new PersonForm())).withSelfRel());
		return new ResponseEntity<>(personV2, HttpStatus.OK);
	} 
	
	@RequestMapping(value="/save",method= RequestMethod.POST)
	public ResponseEntity<?> create(@Valid @RequestBody PersonForm person){
		log.info("POST");
		PersonDTO person_new = personService.create(person);
		PersonDTOv2 personV2 = new PersonDTOv2(person_new);
		
		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTOv2>(personV2,HttpStatus.CREATED);
	}
}
